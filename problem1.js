
function getEmailAddress(dataset) {
    try {
        // Check if dataset is empty
        if (dataset.length === 0) {
            throw new Error('Dataset is empty');
        }

        // Initialize an empty array to store the email addresses
        const emailAddresses = [];

        // Iterate over each object in dataset
        for (let index = 0; index < dataset.length; index++) {
            // Push the email property of the current object to the emailAddresses array
            emailAddresses.push(dataset[index].email);
        }

        // Return the array of email addresses
        return emailAddresses;
    } catch (error) {
        console.error('Error in getEmailAddress function:', error.message);
        return null;
    }
}

module.exports = getEmailAddress;
