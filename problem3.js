//   Problem 3
//   Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

function isStudentFromAustralia(dataset){
try{   
        // Check if dataset is empty
        if (dataset.length === 0) {
            throw new Error('Dataset is empty');
        }

        const result = [];
        for(let index = 0; index < dataset.length; index++){
              if(dataset[index].isStudent === true && dataset[index].country === 'Australia'){
              result.push(dataset[index].name);
        }
    }
    return result;
}
catch (error) {
    console.error('Error in isStudentFromAustralia function:', error.message);
    return null;
}
}

module.exports = isStudentFromAustralia;