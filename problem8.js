//    Problem 8
//    Implement a loop to access and log the city and country of each individual in the dataset.

function printCityAndCountry(dataset){
try {
//  Check if dataset is empty
    if (dataset.length === 0) {
        throw new Error('Dataset is empty');
    }  

    const result = [];
    for(let index = 0; index < dataset.length; index++){
        result.push(`City: ${dataset[index].city} and Country: ${dataset[index].country}`);
    }
    return result;
}
catch(error){
    console.log('Error in printCityAndCountry function', error.message);
}
}

module.exports = printCityAndCountry;