// Problem 6
// Create a function to retrieve and display the first hobby of each individual in the dataset.

function dislayFirstHobby(dataset){
try {
//  Check if dataset is empty
    if (dataset.length === 0) {
    throw new Error('Dataset is empty');
            
}     
    
    const result = [];
    for(let index = 0; index < dataset.length; index++){
        result.push(`Name: ${dataset[index].name} and First Hobby: ${dataset[index].hobbies[0]}`);
    }
    return result;
}
catch(error){
    console.log('Error in displayFirstHobby function', error.message);
}
}

module.exports = dislayFirstHobby;