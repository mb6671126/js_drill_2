try{

    let dataset = require("../dataset");
    const isStudentFromAustralia = require('../problem3.js');
    const result = isStudentFromAustralia(dataset);
    console.log(result);

}
catch (error){

    console.error('Error:', error.message);
    
}