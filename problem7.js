//  Problem 7
//  Write a function that accesses and prints the names and email addresses of individuals aged 25.

function printNameAndEmailOfAge(dataset,age){
try {
      
//  Check if id is provided
    if (age === undefined || age === null) {
        throw new Error('Age is required');
    }  

    const result = [];
    for(let index = 0; index < dataset.length; index++){
        if(dataset[index].age === age){
        result.push(`Name: ${dataset[index].name} and Email: ${dataset[index].email}`);
        }
        return result;
    }
}
catch(error){
    console.log('Error in printNameAndEmailOfAge function', error.message);
}
}

module.exports = printNameAndEmailOfAge;