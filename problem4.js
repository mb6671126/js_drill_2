//   Problem 4
//   Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

function printNameAndCity(dataset,index){
try {

//  Check if infex is provided
    if (index === undefined || index === null) {
            throw new Error('Index is required');
    }    

    return `Name: ${dataset[index].name} and City: ${dataset[index].city}`;
    
}
catch(error){
    console.log('Error in printNameAndCity function', error.message);
}
}

module.exports = printNameAndCity;