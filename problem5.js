// Problem 4
// Implement a loop to access and print the ages of all individuals in the dataset.

function printAgesOfAll(dataset){
try {
//     Check if dataset is empty
    if (dataset.length === 0) {
    throw new Error('Dataset is empty');
    
}   
    const result = [];
    for(let index = 0; index < dataset.length; index++){
    result.push(`${dataset[index].name}'s age: ${dataset[index].age}`);
    }
    return result;
}
catch(error){
    console.log('Error in printAgesOfAll function', error.message);
}
}

module.exports = printAgesOfAll;