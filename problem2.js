//  Problem-2:
//  Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.

function getHobbiesById(dataset,age){
    try {
      
        // Check if id is provided
        if (age === undefined || age === null) {
            throw new Error('Age is required');
        }    
        
        for(let index = 0; index < dataset.length; index++){
        
            if(dataset[index].age == age){
            return dataset[index].hobbies;
            }
        }
    }
    catch (error) {
        console.error('Error in getHobbiesById function:', error.message);
        return null;
    }
}

module.exports = getHobbiesById;